<?php

namespace Renegade\EmailBundle\Emailer;

use Symfony\Bundle\SwiftmailerBundle\SwiftmailerBundle;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;

use Symfony\Component\Form\Extension\DependencyInjection\DependencyInjectionExtension;

use Galderma\ContestBundle\Entity\Entrant;


class Emailer
{
    public function __construct()
    {
        $this->swiftmailer = \Swift_Mailer::newInstance(\Swift_MailTransport::newInstance());
    }

    /**
     * Formats a message and then sends it
     *
     * @param $to
     * @param $from
     * @param $subject
     * @param $body
     * @param $plaintext
     * @return array
     */
    public function dispatchMessage($to, $toName, $from, $fromName, $subject, $body, $plaintext, $bcc = '', $bccName ='')
    {
        foreach(func_get_args() as $arg)
        {
            if(!isset($arg) || empty($arg))
            {
                return array('status' => FALSE, 'message' => 'Invalid Argument: '.$arg);
            }
        }

        $message = \Swift_Message::newInstance()
                ->setSubject($subject)
                ->setFrom($from, $fromName)
                ->setTo($to, $toName)
                ->setBody($body,'text/html')
                ->addPart($plaintext,'text/plain');

        if ($bcc != '') {
            $message->addBcc($bcc, $bccName);
        }

        return($this->swiftmailer->send($message)?array('status' => TRUE, 'message' => 'Message Sent to: '.$to): array('status' => FALSE, 'message' => 'Failed to send message to: '.$to));
    }
}
