<?php

namespace Renegade\EmailBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Renegade\Bundle\EmailBundle\Emailer\Emailer;
use Galderma\ContestBundle\Entity\Entrant;
use Galderma\ContestBundle\Entity\Winner;
use Symfony\Component\HttpFoundation\Response;

/**
 * @Route("/admin", options={"i18n" = false})
 */
class DefaultController extends Controller
{
    /**
     * @Route("/email/")
     * @Template()
     */
    public function indexAction()
    {
        return $this->render("RenegadeEmailBundle:Default:index.html.twig", array("preview" => NULL,"actions" => NULL));
    }

    /**
     * @Route("/email/{id}/preview/{message_type}")
     */
    public function previewAction(Winner $winner, $message_type)
    {
        $format = $message_type == 0 ? 'text/html' : 'text/plain';
        $entrant = $winner->getEntrant();
        $message = $this->getMessage($winner, $format);

        $actions = array(
            'cancel' => array('class' => 'btn btn-danger btn-large','link' => $this->generateUrl('galderma_contest_admin_winners'), 'text' => 'Cancel'),
            'send' => array('class' => 'btn btn-info btn-large', 'link' => $this->generateUrl('renegade_email_default_send', array('id' => $winner->getId())), 'text' => 'Send'),
            'edit' => array('class' => 'btn btn-warning btn-large',
                'link' => $this->generateUrl('galderma_contest_admin_entrantedit', array(
                    'id' => $entrant->getId(),
                    'ref' => $this->generateUrl('renegade_email_default_preview', array(
                        'id' => $winner->getId(),
                        'message_type' => $message_type
                    ))
                )),
                'text' => 'Edit Entrant'),
        );

        return $this->render('RenegadeEmailBundle:Default:index.html.twig', array(
            'format' => $format,
            'preview' => $message,
            'actions' => $actions,
        ));
    }

    /**
     * Return Formatted message
     *
     * @param \Galderma\ContestBundle\Entity\Winner $winner
     * @param null $format [optional - 'text/html','text/plain']
     * @return \Symfony\Component\HttpFoundation\Response
     */

    public function getMessage(Winner $winner, $format = NULL)
    {
        $entrant = $winner->getEntrant();
        $locale = ($entrant->getLocale() == 'fr' ? 'fr' : 'en');
        $format = ($format == "text/plain" ? "text" : "html");

        $renderTemplate = str_replace(array('%locale%', '%format%'), array($locale, $format), 'RenegadeEmailBundle:Templates:email.%format%.%locale%.html.twig');
        $message = $this->render($renderTemplate, array(
            'entrant' => $entrant,
            'question' => $this->getQuestion(),
            'title' => 'WINNER! Cetaphil® DermaControl™ product gift set.',
            'date' => $winner->getDate()->format('F jS'),
        ));

        return (isset($message))?$message->getContent():NULL;
    }

    /**
     * Return a basic math problem
     *
     * @return string
     */

    private function getQuestion()
    {
       return "100÷20x2+2";
    }

    /**
     * @Route("/email/{id}/send/")
     *
     * @param \Galderma\ContestBundle\Entity\Winner $winner
     */
    public function sendAction(Winner $winner)
    {
        $entrant = $winner->getEntrant();
        $mailer = new Emailer();
        $dispatch = $mailer->dispatchMessage(
            $entrant->getEmail(),
            $entrant->getFirstName(),
            $this->container->getParameter('email_from_address'),
            $this->container->getParameter('email_from_name'),
            'WINNER! Cetaphil® DermaControl™ product gift set',
            $this->getMessage($winner,"text/html"),
            $this->getMessage($winner,"text/plain"),
            $this->container->getParameter('email_bcc_address'),
            $this->container->getParameter('email_bcc_name')
        );
        $this->_setFlash( ($dispatch['status'])?'success':'error',$dispatch['message']);
        return $this->redirect($this->generateUrl('galderma_contest_admin_winners'));
    }

    public function _setFlash($type,$message)
    {
        $this->getRequest()->getSession()->getFlashBag()->add($type,$message);
    }


}
